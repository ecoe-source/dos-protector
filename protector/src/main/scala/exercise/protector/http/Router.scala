package exercise.protector.http

import akka.NotUsed
import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.{ActorRef, Scheduler}
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.scaladsl.Flow
import akka.util.Timeout

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._


object Router {

  implicit val timeout: Timeout = Timeout(30 seconds)

  def ipCheck(detective: ActorRef[Detective.Request])(implicit ec: ExecutionContext, scheduler: Scheduler): Route = {
    get {
      handleWebSocketMessages(Flow[Message].via(validateAndRegister(detective)))
    }
  }

  def validateAndRegister(detective: ActorRef[Detective.Request])(implicit ec: ExecutionContext, scheduler: Scheduler):
  Flow[Message, Message, NotUsed] = {
    Flow[Message]
      .mapAsync[Detective.Reply](50) {
        case TextMessage.Strict(txt) => detective.ask { ref =>
          Detective.Request(ip = txt, replyTo = ref)
        }
        case _ => throw new Exception("Unexpected remote address message")
      }.async
      .mapConcat {
        case Detective.Reply(Some(ip)) => TextMessage.Strict(ip) :: Nil
        case Detective.Reply(None) => Nil
      }
  }

}