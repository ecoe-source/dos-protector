package exercise.protector.http

import java.util.concurrent.atomic.AtomicInteger

import akka.actor.typed._
import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.settings.ServerSettings
import akka.util.{ByteString, Timeout}
import com.typesafe.config.ConfigFactory

import scala.collection.JavaConverters._
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}


object Protector extends App {

  val system: ActorSystem[SpawnProtocol.Command] = ActorSystem(Guardian(), "protector")
  implicit val scheduler: Scheduler = system.scheduler
  implicit val executionContext: ExecutionContext = system.executionContext
  implicit val timeout: Timeout = Timeout(3.seconds)
  implicit val classicSystem: akka.actor.ActorSystem = system.classicSystem // only classic supported for akka-http

  val port: Int = ConfigFactory.load().getInt("env.protector.port")
  val blacklistIps: Seq[String] = ConfigFactory.load().getStringList("env.protector.limits.blacklist").asScala
  val requestsPerMinute: Int = ConfigFactory.load().getInt("env.protector.limits.requests-per-minute")
  val historySeconds: Int = ConfigFactory.load().getInt("env.protector.limits.history-seconds")

  val defaultSettings = ServerSettings(classicSystem)
  val pingCounter = new AtomicInteger()
  val customWebsocketSettings =
    defaultSettings.websocketSettings
      .withPeriodicKeepAliveData(() => ByteString(s"debug-${pingCounter.incrementAndGet()}"))

  val keepAliveSettings =
    defaultSettings.withWebsocketSettings(customWebsocketSettings)


  val detective: ActorRef[Detective.Request] = Await.result(
    system.ask(
      ref => SpawnProtocol.Spawn(
        behavior = Detective(Detective.Limits(requestsPerMinute = requestsPerMinute, historySeconds = historySeconds), blacklistIps),
        name = "arbiter",
        props = Props.empty,
        replyTo = ref
      )
    ),
    10 seconds
  )

  Http().bindAndHandle(
    handler = Router.ipCheck(detective),
    interface = "0.0.0.0",
    port = port,
    settings = keepAliveSettings
  )
}

// allows spawning new actors with reference to implicit system for initializing websocket
object Guardian {
  def apply(): Behavior[SpawnProtocol.Command] = Behaviors.setup(_ => SpawnProtocol())
}
