package exercise.protector.http


import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import akka.stream.Materializer
import akka.stream.scaladsl.Source
import akka.util.Timeout

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._


trait Detective {

  implicit val timeout: Timeout = Timeout(30 seconds)

  sealed trait Command
  case class Request(ip: String, replyTo: ActorRef[Reply]) extends Command
  case class CleanHistory(currentTimeMillis: Long) extends Command
  case class Reply(ip: Option[String])

  case class IpStats(requestsPerMinute: Int = 0, totalCount: Int = 1, firstSet: Long, lastReceived: Option[Long] = None, malicious: Boolean = false)
  case class Limits(requestsPerMinute: Int, historySeconds: Int)


  def apply(limits: Limits, blacklist: Seq[String] = Seq.empty[String])
           (implicit executionContext: ExecutionContext, materializer: Materializer):
  Behavior[Command] = {
    Behaviors.setup{ context =>
      val now = () => System.currentTimeMillis()
      Source
        .tick(initialDelay = 2.second, interval = 2.second, now)
        .runForeach(now => context.self.narrow[CleanHistory] ! CleanHistory(now()))

      next(limits, initiallyBanned(blacklist, now()), pending = 0)
    }

  }

  def next(limits: Limits, ipStats: Map[String, IpStats], pending: Int)(implicit materializer: Materializer):
  Behavior[Command] =
    Behaviors.receive { (context, command) =>
      command match {

        case Request(ip, replyTo) =>
          println(s"receive message: $ip")
          val latestStats = ipStats.get(ip) match {

            case Some(stats) =>
              val updatedStats = updateStats(stats, System.currentTimeMillis(), limits)
              ipStats + (ip -> updatedStats)

            case None =>
              val received = System.currentTimeMillis()
              ipStats + (ip -> IpStats(firstSet = received, lastReceived = Some(received)))
          }

          val response = if (latestStats(ip).malicious) Some(ip) else None
          replyTo ! Reply(response)

          next(limits, latestStats, pending)

        case CleanHistory(now) =>
          println(s"cleaning history, removed ${ipStats.size - cleanIpHistory(ipStats, now, limits).size}")
          next(limits, cleanIpHistory(ipStats, now, limits), pending)
      }
    }

  def updateStats(stats: IpStats, lastReceivedMillis: Long, limits: Limits): IpStats = {
    // ips that were never received are permanently blacklisted in application.conf configuration
    stats.lastReceived.fold(stats) { _ =>
      val sessionMinutes = (lastReceivedMillis - stats.firstSet) * millisToMinutes
      val averageFrequency = ((stats.totalCount + 1) / sessionMinutes).round.toInt
      println(s"Detected frequency: $averageFrequency over $sessionMinutes min")
      stats.copy(
        requestsPerMinute = averageFrequency,
        totalCount = stats.totalCount + 1,
        lastReceived = Some(lastReceivedMillis),
        malicious = averageFrequency > limits.requestsPerMinute
      )
    }
  }

  def cleanIpHistory(ips: Map[String, IpStats], now: Long, limits: Limits): Map[String, IpStats] =
    ips.filterNot(oldIps(now, limits))

  def oldIps(now: Long, limits: Limits): ((String, IpStats)) => Boolean = {
    case (ip, stats) => stats.lastReceived.fold(false)(now - _ > limits.historySeconds / 0.001)
  }

  def initiallyBanned(blacklist: Seq[String], now: Long): Map[String, IpStats] = blacklist.map(
    ip => ip -> IpStats(malicious = true, firstSet = now)
  ).toMap

  val millisToMinutes: Double = 0.001/60
}

object Detective extends Detective