package exercise.protector.http

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.actor.typed.Scheduler
import akka.http.scaladsl.testkit.{ScalatestRouteTest, WSProbe}
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterEach, Matchers, WordSpec}

import scala.concurrent.duration._


class WebsocketFrequencyExceedanceSpec extends WordSpec
  with Matchers
  with BeforeAndAfterEach
  with ScalatestRouteTest {

  var testKit: ActorTestKit = ActorTestKit()
  implicit val scheduler: Scheduler = testKit.scheduler

//  override def beforeEach: Unit = testKit: ActorTestKit = ActorTestKit()
  override def afterAll: Unit = testKit.shutdownTestKit()

  "DetectiveWebsocket" should {

    "send malicious ip that exceeds frequency" in {
      val wsClient: WSProbe = WSProbe()
      val ip = "192.168.3.12"
      val detectiveMock = testKit.spawn(Detective(Detective.Limits(requestsPerMinute = 1, 100), blacklist = Seq()))

      WS("/", wsClient.flow) ~> Router.ipCheck(detectiveMock) ~> check {
        isWebSocketUpgrade shouldEqual true

        wsClient.sendMessage(ip)
        wsClient.sendMessage(ip)
        wsClient.expectMessage(ip)

        wsClient.sendCompletion()
        wsClient.expectCompletion()
      }
      testKit.stop(detectiveMock)
    }
  }
}
