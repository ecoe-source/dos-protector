package exercise.protector.http


import org.scalatest.{Matchers, WordSpecLike}
import scala.concurrent.duration._


class DetectiveSpec extends WordSpecLike
  with Matchers
  with Detective {

  "convert millis to minutes" in {
    millisToMinutes * 60000 shouldEqual 1
  }

  "cleanIpHistory" should {

    "purge traffic older than history limit" in {
      val now = System.currentTimeMillis()
      val blacklisted = Map("192.168.0.1" -> IpStats(lastReceived = Some(now - 2.minutes.toMillis), firstSet = now))
      cleanIpHistory(blacklisted, now, limits = Limits(requestsPerMinute = 3, historySeconds = 1)) shouldEqual Map.empty[String, IpStats]
    }

    "not purge traffic that's younger than the history limit" in {
      val now = System.currentTimeMillis()
      val blacklisted = Map("192.168.0.1" -> IpStats(lastReceived = Some(now - 1.minutes.toMillis), firstSet = now))
      cleanIpHistory(blacklisted, now, limits = Limits(requestsPerMinute = 3, historySeconds = 120)) shouldEqual blacklisted
    }

    "not purge traffic that's not tracking history" in {
      val now = System.currentTimeMillis()
      val blacklisted = initiallyBanned(Seq("192.168.0.1"), now)
      cleanIpHistory(blacklisted, now, limits = Limits(requestsPerMinute = 3, historySeconds = 0)) shouldEqual blacklisted
    }
  }

  "updateStats" should {

    "flag frequencies as malicious that exceed limit" in {
      val now = System.currentTimeMillis()
      val initial = IpStats(totalCount = 2, firstSet = now - 20.seconds.toMillis, lastReceived = Some(now))
      updateStats(initial, now, limits = Limits(requestsPerMinute = 2, historySeconds = 1)) shouldEqual
      initial.copy(requestsPerMinute = 9, totalCount = 3, lastReceived = Some(now), malicious = true)
    }

    "don't flag frequencies at the limit" in {
      val now = System.currentTimeMillis()
      val initial = IpStats(totalCount = 2, firstSet = now - 20.seconds.toMillis, lastReceived = Some(now))
      updateStats(initial, now, limits = Limits(requestsPerMinute = 9, historySeconds = 1)) shouldEqual
      initial.copy(requestsPerMinute = 9, totalCount = 3, lastReceived = Some(now), malicious = false)
    }

    "don't flag frequencies below the limit" in {
      val now = System.currentTimeMillis()
      val initial = IpStats(totalCount = 2, firstSet = now - 20.seconds.toMillis, lastReceived = Some(now))
      updateStats(initial, now, limits = Limits(requestsPerMinute = 10, historySeconds = 1)) shouldEqual
      initial.copy(requestsPerMinute = 9, totalCount = 3, lastReceived = Some(now), malicious = false)
    }

    "blacklisted ips remain malicious" in {
      val now = System.currentTimeMillis()
      val blacklisted = initiallyBanned(Seq("192.168.0.1"), now).values.head
      updateStats(blacklisted, now, limits = Limits(9999,9999)) shouldEqual blacklisted
    }

  }

}
