package exercise.protector.http


import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.actor.typed.Scheduler
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest, WSProbe}
import akka.testkit.TestDuration
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterEach, Matchers, WordSpec}

import scala.concurrent.duration._


class WebsocketBlacklistSpec extends WordSpec
  with Matchers
  with BeforeAndAfterEach
  with ScalatestRouteTest {

  var testKit: ActorTestKit = ActorTestKit()
  implicit val scheduler: Scheduler = testKit.scheduler
  implicit val timeout: Timeout = Timeout(5 minutes)

//  override def beforeEach: Unit = testKit: ActorTestKit = ActorTestKit()
  override def afterAll: Unit = testKit.shutdownTestKit()

  "DetectiveWebsocket" should {

    "send blacklisted ip" in {
      val wsClient: WSProbe = WSProbe()
      val ip = "192.168.3.12"
      val detectiveMock = testKit.spawn(Detective(Detective.Limits(100, 100), blacklist = Seq(ip)))

      WS("/", wsClient.flow) ~> Router.ipCheck(detectiveMock) ~> check {
        isWebSocketUpgrade shouldEqual true

        wsClient.sendMessage(ip)
        Thread.sleep(1000)
        wsClient.expectMessage(ip)

        wsClient.sendCompletion()
        wsClient.expectCompletion()
      }
      testKit.stop(detectiveMock)
    }
  }
}
