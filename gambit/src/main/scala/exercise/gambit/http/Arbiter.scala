package exercise.gambit.http

import akka.Done
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.ws.{Message, TextMessage, WebSocketRequest, WebSocketUpgradeResponse}
import akka.stream.scaladsl.{Flow, Keep, Sink, Source, SourceQueueWithComplete}
import akka.stream.{Materializer, OverflowStrategy}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}


object Arbiter {

  sealed trait Command
  case class FlowContext(trafficQueue: SourceQueueWithComplete[TextMessage], upgradeResponse: Future[WebSocketUpgradeResponse], closed: Future[Done])
  case class Request(ip: String, replyTo: ActorRef[Reply]) extends Command
  case class Ban(ip: String) extends Command
  case class Reply(ipBenign: Boolean)


  def apply(flow: ActorContext[Command] => FlowContext, initialBanned: Seq[String] = Seq())(implicit materializer: Materializer): Behavior[Command] = {

    Behaviors.setup { context =>

      val flowContext = flow(context)

      val connected = flowContext.upgradeResponse.flatMap { upgrade =>
        if (upgrade.response.status == StatusCodes.SwitchingProtocols) {
          Future.successful(Done)
        } else {
          throw new RuntimeException(s"Connection failed: ${upgrade.response.status}")
        }
      }

      flowContext.trafficQueue.watchCompletion()
        .onComplete {
          case Success(result) => {
            println(s"FATAL: Stream completed without errors ($result)")
            context.system.terminate()
          }
          case Failure(error) => {
            println(s"FATAL: Stream completed with errors: $error")
            context.system.terminate()
          }
        }

      connected.onComplete( result => println(result))

      flowContext.closed.foreach(_ => println("closed"))

      next(flowContext.trafficQueue, initialBanned, pending = 0)
    }
  }

  def next(trafficQueue: SourceQueueWithComplete[TextMessage], bannedIps: Seq[String], pending: Int)(implicit materializer: Materializer):
  Behavior[Command] =
    Behaviors.receive { (context, command) =>

      command match {
        case Request(ip, replyTo) => {
          println(s"message: $ip and banned: $bannedIps")
          if (bannedIps.contains(ip)) {
            replyTo ! Reply(ipBenign = false)
            Behaviors.same
          } else {
            // presume it initially won't be banned, which is eventually consistent once the protector responds
            // alternatively, you could wait to reply until the command where the protector has updated the banned ips
            trafficQueue.offer(TextMessage(ip))
            replyTo ! Reply(ipBenign = true)

            next(trafficQueue, bannedIps, pending + 1)
          }
        }

        case Ban(ip) => {
          next(trafficQueue, bannedIps.+:(ip), pending - 1)
        }
      }
    }

  def receiveNewBan(context: ActorContext[Command]): Sink[Message, Future[Done]] = Sink.foreach[Message] {
    case message: TextMessage.Strict =>
      println(s"processed banned ip from protector: $message")
      context.self ! Ban(message.text)
    case _ => throw new Exception("Unexpected message from DoS protector")

  }

  def ipBan(uri: String, port: Int, queueBuffer: Int)(implicit actorSystem: akka.actor.ActorSystem, materializer: Materializer):
  ActorContext[Command] => FlowContext = {

            println(s"ws://$uri:$port")
    val webSocketFlow: Flow[Message, Message, Future[WebSocketUpgradeResponse]] =
      Http().webSocketClientFlow(WebSocketRequest(s"ws://$uri:$port"))

    val traffic: Source[TextMessage, SourceQueueWithComplete[TextMessage]] = Source.queue[TextMessage](queueBuffer, OverflowStrategy.backpressure)

    (context: ActorContext[Command]) => {
      val ((trafficQueue, upgradeResponse), closed) = traffic
        .viaMat(webSocketFlow)(Keep.both) // keep the materialized Future[WebSocketUpgradeResponse]
        .toMat(receiveNewBan(context))(Keep.both) // also keep the Future[Done]
        .run()

      FlowContext(trafficQueue, upgradeResponse, closed)
    }

  }
}
