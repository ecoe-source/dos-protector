package exercise.gambit.http

import akka.actor.typed._
import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.util.Timeout
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}


object Gambit extends App {

  val system: ActorSystem[SpawnProtocol.Command] = ActorSystem(Guardian(), "gambit")
  implicit val scheduler: Scheduler = system.scheduler
  implicit val executionContext: ExecutionContext = system.executionContext
  implicit val timeout: Timeout = Timeout(3.seconds)
  implicit val classicSystem: akka.actor.ActorSystem = system.classicSystem // only classic supported for akka-http

  val uri: String = ConfigFactory.load().getString("env.protector.uri")
  val port: Int = ConfigFactory.load().getInt("env.protector.port")
  val queueBuffer: Int = ConfigFactory.load().getInt("env.protector.buffer")

  val arbiter: ActorRef[Arbiter.Request] = Await.result(
    system.ask(
      ref => SpawnProtocol.Spawn(
        behavior = Arbiter(Arbiter.ipBan(uri, port, queueBuffer)),
        name = "arbiter",
        props = Props.empty,
        replyTo = ref
      )
    ),
    10 seconds
  )

  Http().bindAndHandle(
    handler = Router.validTrafficResponse(arbiter),
    interface = "0.0.0.0",
    port = ConfigFactory.load().getInt("env.gambit.port")
  )
}

// allows spawning new actors with reference to implicit system for initializing websocket
object Guardian {
  def apply(): Behavior[SpawnProtocol.Command] = Behaviors.setup(_ => SpawnProtocol())
}
