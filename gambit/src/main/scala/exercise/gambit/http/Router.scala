package exercise.gambit.http

import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.{ActorRef, Scheduler}
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{complete, _}
import akka.http.scaladsl.server._
import akka.util.Timeout

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success


object Router {

  implicit val timeout: Timeout = Timeout(30 seconds)

  def validTrafficResponse(arbiter: ActorRef[Arbiter.Request])(implicit ec: ExecutionContext, scheduler: Scheduler): Route = {
    get {
      extractClientIP { remoteAddress =>
        benignTraffic(remoteAddress, arbiter).apply {
          complete(HttpResponse(StatusCodes.OK))
        }
      }
    }
  }

  def benignTraffic(remoteAddress: RemoteAddress, arbiter: ActorRef[Arbiter.Request])(implicit ex: ExecutionContext, scheduler: Scheduler): Directive0 = {
    askBenign(remoteAddress, arbiter).flatMap { reply =>
      if (reply.ipBenign) {
        pass
      } else {
        reject(ValidationRejection("Malicious client behavior detected."))
      }
    }
  }

  def askBenign(remoteAddress: RemoteAddress, arbiter: ActorRef[Arbiter.Request])(implicit ex: ExecutionContext, scheduler: Scheduler): Directive1[Arbiter.Reply] = {
    onComplete(
      arbiter.ask(
        ref => Arbiter.Request(ip = remoteAddress.toOption.map(_.getHostAddress).getOrElse("unknown"), replyTo = ref)
      ): Future[Arbiter.Reply]
    ).flatMap {
      case Success(reply) => provide(reply)
      case _ => reject(ValidationRejection("Failed to validate client remote address."))
    }
  }
}