package exercise.gambit.http

import java.net.InetAddress

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.actor.typed.Scheduler
import akka.http.scaladsl.model.headers.`Remote-Address`
import akka.http.scaladsl.model.{HttpResponse, RemoteAddress, StatusCodes}
import akka.http.scaladsl.server.{RouteResult, ValidationRejection}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{Matchers, WordSpec}

import scala.collection.mutable
import scala.concurrent.Future


class RouterSpec extends WordSpec
  with Matchers
  with ArbiterMock
  with ScalatestRouteTest {

  val testKit: ActorTestKit = ActorTestKit()
  implicit val scheduler: Scheduler = testKit.scheduler

  override def afterAll: Unit = testKit.shutdownTestKit()

  val ip = "192.168.3.12"
  val remoteAddress: RemoteAddress.IP = RemoteAddress(InetAddress.getByName(ip))

  "banned ip" should {

    "be malicious" in {
      val arbiterMock = testKit.spawn(Arbiter(mockFlow, initialBanned = mutable.Seq(ip)))
      Router.benignTraffic(remoteAddress, arbiterMock).apply { reply =>
        reply shouldEqual Arbiter.Reply(ipBenign = false)
        Future(RouteResult.Complete(HttpResponse()))
      }
    }

    "rejected for malicious" in {
      val arbiterMock = testKit.spawn(Arbiter(mockFlow, initialBanned = mutable.Seq(ip)))
      Get("/").withHeaders(`Remote-Address`(remoteAddress)) ~> Router.validTrafficResponse(arbiterMock) ~> check {
        rejections.head shouldBe a [ValidationRejection]
      }
    }
  }

  "not banned ip" should {

    "be benign" in {
      val arbiterMock = testKit.spawn(Arbiter(mockFlow))
      Router.benignTraffic(remoteAddress, arbiterMock).apply { reply =>
        reply shouldEqual Arbiter.Reply(ipBenign = true)
        Future(RouteResult.Complete(HttpResponse()))
      }
    }

    "OK if benign" in {
      val arbiterMock = testKit.spawn(Arbiter(mockFlow))
      Get("/").withHeaders(`Remote-Address`(remoteAddress)) ~> Router.validTrafficResponse(arbiterMock) ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[String] shouldEqual ""
      }
    }
  }

  "not yet banned ip" should {

    val arbiterMock = testKit.spawn(Arbiter(mockFlow))
    "OK first time" in {
      Get("/").withHeaders(`Remote-Address`(remoteAddress)) ~> Router.validTrafficResponse(arbiterMock) ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[String] shouldEqual ""
      }
    }

    "Rejected once ip is banned" in {
      Get("/").withHeaders(`Remote-Address`(remoteAddress)) ~> Router.validTrafficResponse(arbiterMock) ~> check {
        rejections.head shouldBe a[ValidationRejection]
      }
    }

    "Stays rejected once ip is banned" in {
      Get("/").withHeaders(`Remote-Address`(remoteAddress)) ~> Router.validTrafficResponse(arbiterMock) ~> check {
        rejections.head shouldBe a[ValidationRejection]
      }
    }
  }

}
