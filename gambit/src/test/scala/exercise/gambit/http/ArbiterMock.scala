package exercise.gambit.http

import akka.Done
import akka.actor.typed.scaladsl.ActorContext
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.ws.{TextMessage, ValidUpgrade}
import akka.stream.{Materializer, OverflowStrategy}
import akka.stream.scaladsl.{Keep, Source, SourceQueueWithComplete}
import exercise.gambit.http.Arbiter.FlowContext

import scala.concurrent.{ExecutionContext, Future}

trait ArbiterMock {

  def mockFlow(implicit ec: ExecutionContext, mat: Materializer):
  ActorContext[Arbiter.Command] => Arbiter.FlowContext = {

    (context: ActorContext[Arbiter.Command]) => {
      val mockTraffic: SourceQueueWithComplete[TextMessage] = Source
        .queue[TextMessage](bufferSize = 10, OverflowStrategy.backpressure)
        .toMat(Arbiter.receiveNewBan(context))(Keep.left)
        .run()

      FlowContext(
        trafficQueue = mockTraffic,
        upgradeResponse = Future(ValidUpgrade(HttpResponse(), None)),
        Future(Done)
      )
    }
  }



}
