# dos-protector

Exercise checking out Akka's new 2020 release of [Typed Actors](https://doc.akka.io/docs/akka/current/typed/actors.html) along with Docker, Akka Streams/HTTP, and SBT.
 
## How to Run
###### ⚠ You need [SBT](https://www.scala-sbt.org/download.html) and [Docker](https://docs.docker.com/engine/install/) installed, but can also run in Intellij (right click and run the .scala file with the same title as the module)

1. Download and `cd` into this repository's root directory
1. Compose the docker containers ("gambit" server and its "protector"):
   ```shell
   sbt docker-up
   ```
1. Make some requests to the gambit server:
   ```shell
   curl -v localhost:9001
   ```
  
If you exceed the configured request rate limit (see `protector/src/main/resources/application.conf`) you will be permanently banned! The default limit is pretty basic (just for example purposes): a single remote address can't exceed 2000 requests per minute. There is also a configurable blacklist of IP addresses for manual blocking of known offenders.
  
## About

Purely an academic exercise to produce an interactive, scalable, and fun concept of Akka's latest 2020 Typed Actors release. 

### Academic Value

This exercise focuses on separating concerns of a simple web service (the vulnerable "gambit") from a DoS (Denial of Service) attack detection by the so-called "protector" service. In doing so, this both saves CPU of the gambit server and enables more modular design so that the protector can be reused for other use-cases (other vulnerable web servers, etc).

This is achieved by the gambit running in its own container and then sending remote addresses efficiently via webSockets to the "protector" container for further processing. Should the protector notice specific IP addresses that are too frequently making requests, it will inform the web server via the already established webSocket connection to (hopefully) mitigate a DoS (Denial of Service) attack.

✮ Achievements:
  - Efficient Akka Stream processing of remote addresses using webSockets and _newly released_ Typed Actors
  - Modular Dockerized microservice architecture
  - Turn-key SBT build and deployment
  
⚒ Future Improvements:
  - Include an Nginx reverse proxy server, with additional [DDoS Protection](https://www.nginx.com/blog/mitigating-ddos-attacks-with-nginx-and-nginx-plus/)
  - Monitor more specific DoS behaviors rather than just simple threshold exceedances
  - Using strictly `Tell` with `Actor` communication rather than `Ask` for even more server capacity (to take on bigger attacks)
  - Make use of Docker Networks and Docker Compose rather than just running with `--net=host`

### Production Value

In a production environment I would rely instead on a load balancer with a firewall or pay a CDN for an even larger capacity to take on the biggest and baddest attacks.

In terms of software, I would opt instead for a javaagent approach, such as Kamon Metrics, and simply monitor and alert application health metrics for even more surveillance on top of commercial transport layer protection. In addition, any vulnerable service should require (multi-factor) user authentication (with delayed login reattempts) before sensitive requests are authorized.
