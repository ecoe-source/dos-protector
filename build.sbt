import com.typesafe.sbt.packager.docker._
import sbt.Keys._
import sbt.{Compile, addCommandAlias, file, project, taskKey}


name := "dos-protector"
scalaVersion := "2.12.11"

lazy val root = (project in file(".")).settings(runProtectorSetting, runGambitSetting)

lazy val gambit = (project in file("gambit"))
  .enablePlugins(
    // includes the ability to build docker images
    JavaAppPackaging,
    // generates an executable "ash" script (limited subset of bash) for the light weight Alpine base image
    AshScriptPlugin
  )
  .settings(Dependencies.base)
  .settings(
    name := "gambit-service",
    packageName in Docker := "akka-http-dos-gambit",
    version in Docker := "0.0.1",
    dockerExposedPorts ++= Seq(9001, 9001),
  )

lazy val protector = (project in file("protector"))
  .enablePlugins(
    // includes the ability to build docker images
    JavaAppPackaging,
    // generates an executable "ash" script (limited subset of bash) for the light weight Alpine base image
    AshScriptPlugin
  )
  .settings(Dependencies.base)
  .settings(
    name := "protector-service",
    packageName in Docker := "akka-http-dos-protector",
    version in Docker := "0.0.1",
    dockerExposedPorts ++= Seq(9002, 9002),

    libraryDependencies ++= Seq(
      // provided dependency for websocket test support
      "com.typesafe.akka" %% "akka-stream-testkit" % Dependencies.akkaVersion % Test
    )
  )

lazy val runProtector = taskKey[Unit]("Runs docker compose after images have been published locally")
lazy val runProtectorSetting = runProtector := {
  import scala.sys.process._
  println("Starting Microservice containers for images that SBT already built and created!")
  Seq("docker", "run", "--detach", "--net=host", "--name", "protector", "-p", "9002:9002", "-it", "akka-http-dos-protector:0.0.1") !
}

lazy val runGambit = taskKey[Unit]("Runs docker compose after images have been published locally")
lazy val runGambitSetting = runGambit := {
  import scala.sys.process._
  println("Starting Microservice containers for images that SBT already built and created!")
  Seq("docker", "run", "--detach", "--net=host", "--name", "gambit", "-p", "9001:9001", "-it", "akka-http-dos-gambit:0.0.1") !
}

addCommandAlias(
  name = "docker-up",
  value = ";protector/clean" +
    ";protector/test" +
    ";protector/docker:publishLocal" +
    ";gambit/clean" +
    ";gambit/test" +
    ";gambit/docker:publishLocal" +
    ";root/runProtector" +
    ";root/runGambit"
)

addCommandAlias(
  name = "docker-protector",
  value = ";root/runProtector"
)

addCommandAlias(
  name = "docker-gambit",
  value = ";root/runGambit"
)
